package com.example.homework5.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.homework5.entity.CategoryDao;

import java.util.List;

@Dao
public interface CategoryInterface {

    @Insert()
    public void insertCategory(CategoryDao... categoryDao);

    @Query("select categoryBook from categorydao")
    List<String> findAllCategory();

    @Query("select * from categorydao")
    List<CategoryDao> findAll();

    @Delete()
    public void deleteCategory(CategoryDao... categoryDao);
}
