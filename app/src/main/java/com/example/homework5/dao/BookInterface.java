package com.example.homework5.dao;

import androidx.annotation.InspectableProperty;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.homework5.entity.BookDao;

import java.util.List;

@Dao
public interface BookInterface {

    @Insert()
    void insertBook(BookDao... bookDao);

    @Query("select * from bookdao")
    List<BookDao> findAll();

    @Query("select * from bookdao where id = :id")
    BookDao findOne(int id);

    @Delete()
    void deleteBook(BookDao bookDao);

    @Update
    void updateBook(BookDao bookDao);

    @Query("select * from bookdao where title like :search")
    List<BookDao> searchAll(String search);
}
