package com.example.homework5.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.homework5.entity.BookDao;
import com.example.homework5.R;
import com.example.homework5.ViewActivity;
import com.example.homework5.sharedata.GetPosition;



import java.util.List;

public class RecyclerViewBookAdapter extends RecyclerView.Adapter<RecyclerViewBookAdapter.MyViewHolder> implements GetPosition {


    private List<BookDao> bookDaoList;
    Context context;

    PassingData passingData;

    public RecyclerViewBookAdapter(PassingData passingData,List<BookDao> bookDaoList,Context context) {
        this.bookDaoList = bookDaoList;
        this.context = context;
        this.passingData = passingData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.recycler_book_item,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        holder.title_book.setText(bookDaoList.get(position).getTitle());
        holder.description_book.setText(bookDaoList.get(position).getDescription());
        holder.price_book.setText(String.valueOf(bookDaoList.get(position).getPrice()));
        holder.dec_price_book.setText(String.valueOf(bookDaoList.get(position).getDecPrice()));
        Glide.with(context).load(bookDaoList.get(position).getThumbnail()).into(holder.imageView);

        holder.option_digit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupMenu popupMenu = new PopupMenu(context,holder.option_digit);
                popupMenu.inflate(R.menu.card_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()){
                            case R.id.edit:
                                passingData.onPassingPosition(bookDaoList.get(position).getId());
                                break;
                            case R.id.view:
                                Intent intent = new Intent(context,ViewActivity.class);
                                intent.putExtra("Book",bookDaoList.get(position));
                                context.startActivity(intent);
                                break;
                            case R.id.remove:
                                passingData.onPassingBook(bookDaoList.get(position));
                                notifyDataSetChanged();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return bookDaoList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView title_book,description_book,price_book,dec_price_book,option_digit;
        ImageView imageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            title_book = itemView.findViewById(R.id.title_book);
            description_book = itemView.findViewById(R.id.description_book);
            price_book = itemView.findViewById(R.id.price_book);
            dec_price_book = itemView.findViewById(R.id.dec_price_book);
            imageView = itemView.findViewById(R.id.image_view_book);
            option_digit = itemView.findViewById(R.id.optionDigit);

        }

    }


   /// interface for passing data to Homefragment.class
    public interface PassingData{
        void onPassingPosition(int position);
        void onPassingBook(BookDao bookDao);
    }

}
