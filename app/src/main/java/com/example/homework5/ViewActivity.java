package com.example.homework5;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.example.homework5.entity.BookDao;

public class ViewActivity extends AppCompatActivity {

    BookDao bookDao = null;

    TextView view_title,view_description,view_price,view_total;
    ImageView imageViews;


    public void Init(){
        view_title = findViewById(R.id.view_title);
        view_description = findViewById(R.id.view_description_view);
        view_price = findViewById(R.id.view_price_view);
        view_total = findViewById(R.id.view_total_view);
        imageViews = findViewById(R.id.view_image_view);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        bookDao = (BookDao) getIntent().getSerializableExtra("Book");
        Init();
        setValueToLayout();
        Log.d("TAG", "onCreate: "+bookDao.getThumbnail());
    }

    public void setValueToLayout(){
        view_title.setText(bookDao.getTitle());
        view_description.setText(bookDao.getDescription());
        view_price.setText(String.valueOf(bookDao.getPrice()));
        view_total.setText(String.valueOf(bookDao.getDecPrice()));
        Glide.with(this).load(bookDao.getThumbnail()).into(imageViews);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

}