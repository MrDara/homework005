package com.example.homework5.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class CategoryDao {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String categoryBook;

    public CategoryDao(String categoryBook) {
        this.categoryBook = categoryBook;
    }

    public String getCategoryBook() {
        return categoryBook;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCategoryBook(String categoryBook) {
        this.categoryBook = categoryBook;
    }

    @Override
    public String toString() {
        return "CategoryDao{" +
                "id=" + id +
                ", categoryBook='" + categoryBook + '\'' +
                '}';
    }
}
