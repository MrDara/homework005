package com.example.homework5.entity;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class BookDao implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String title;
    private String description;
    private String thumbnail;
    private int price;
    private int decPrice;

    public BookDao() {
    }

    public BookDao(String title, String description, String thumbnail, int price, int decPrice) {
        this.title = title;
        this.description = description;
        this.thumbnail = thumbnail;
        this.price = price;
        this.decPrice = decPrice;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDecPrice() {
        return decPrice;
    }

    public void setDecPrice(int decPrice) {
        this.decPrice = decPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BookDao{" +
                "title='" + id + '\'' +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", thumbnail=" + thumbnail +
                ", price=" + price +
                ", decPrice=" + decPrice +
                '}';
    }
}
