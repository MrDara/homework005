package com.example.homework5.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.homework5.dao.BookInterface;
import com.example.homework5.dao.CategoryInterface;
import com.example.homework5.entity.BookDao;
import com.example.homework5.entity.CategoryDao;

@Database(entities = {BookDao.class, CategoryDao.class},version = 3)
public abstract class RoomDataBaseConfig extends RoomDatabase {
    public abstract BookInterface bookInterface();
    public abstract CategoryInterface categoryInterface();

    public static RoomDataBaseConfig INSTANCE;

    public static RoomDataBaseConfig getRoomDb(Context context){

        if(INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),RoomDataBaseConfig.class,"db_book")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return INSTANCE;
    }

    public static void destroy(){
        INSTANCE = null;
    }
}
