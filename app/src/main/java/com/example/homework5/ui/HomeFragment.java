package com.example.homework5.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.homework5.MainActivity;
import com.example.homework5.entity.BookDao;
import com.example.homework5.R;
import com.example.homework5.adapter.RecyclerViewBookAdapter;
import com.example.homework5.room.RoomDataBaseConfig;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.app.Activity.RESULT_OK;


public class HomeFragment extends Fragment implements RecyclerViewBookAdapter.PassingData{

    List<BookDao> bookDao = new ArrayList<BookDao>();;
    RecyclerViewBookAdapter recyclerViewBookAdapter;
    int index;
    RecyclerView recyclerView;

    TextView header_dialog;
    EditText title_dialog,price_dialog,discount_dialog;
    Spinner spinner_dialog;

    Button btn_save,btn_cancel;
    FloatingActionButton btn_add_image;
    ImageView imageViews;
    public final static int CHOOSE_REQUEST_CODE = 1;

    Uri uri;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public HomeFragment() {
        // Required empty public constructor
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BlankFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        // get a reference to recycler
        recyclerView = view.findViewById(R.id.recycler_view_book_list);
        // set LinearLayoutManager
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        // this is data for recycler view
        bookDao = RoomDataBaseConfig.getRoomDb(getContext()).bookInterface().findAll();
        if(bookDao == null){
            Toast.makeText(getContext(),"Null",Toast.LENGTH_LONG).show();
        }else{
            recyclerViewBookAdapter = new RecyclerViewBookAdapter(this,bookDao,getContext());
            recyclerView.setAdapter(recyclerViewBookAdapter);
        }
        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setQueryHint("Search Data here .... ");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                List<BookDao> bookDaos = RoomDataBaseConfig.getRoomDb(getContext()).bookInterface().searchAll(newText);
                if (bookDaos.size()>0){
                    recyclerViewBookAdapter = new RecyclerViewBookAdapter(HomeFragment.this,bookDaos,getContext());
                    recyclerView.setAdapter(recyclerViewBookAdapter);
                }
                if(newText == null || newText.isEmpty()){
                    recyclerViewBookAdapter = new RecyclerViewBookAdapter(HomeFragment.this,bookDao,getContext());
                    recyclerView.setAdapter(recyclerViewBookAdapter);
                }
                return false;
            }
        });
    }



    @Override
    public void onPassingPosition(int position) {
        index = position;
        BookDao bookDao = RoomDataBaseConfig.getRoomDb(getContext()).bookInterface().findOne(position);

        if(bookDao == null){
            Toast.makeText(getContext(),"Error Select Book",Toast.LENGTH_LONG).show();
        }else {
            CallDialog(bookDao,position);
        }

    }

    @Override
    public void onPassingBook(BookDao bookDao) {
        RoomDataBaseConfig.getRoomDb(getContext()).bookInterface().deleteBook(bookDao);
        recyclerViewBookAdapter.notifyDataSetChanged();
        getFragmentManager().beginTransaction().detach(this).attach(this).commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        recyclerViewBookAdapter.notifyDataSetChanged();
    }

    public void CallDialog(BookDao bookDao, final int Id){
        AlertDialog.Builder builderDialog;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            builderDialog = new AlertDialog.Builder(getContext(),android.R.style.Theme_Material_Dialog_Alert);
        }else{
            builderDialog = new AlertDialog.Builder(getContext());
        }
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.dailog_custom,null);
        builderDialog.setView(view);
        builderDialog.setCancelable(true);
        final AlertDialog dialog = builderDialog.create();

        header_dialog = view.findViewById(R.id.header_dialog);
        title_dialog = view.findViewById(R.id.edit_titlebook_dialog);
        spinner_dialog = view.findViewById(R.id.spinner_dialog_view);
        price_dialog = view.findViewById(R.id.edit_text_price);
        discount_dialog = view.findViewById(R.id.edit_text_discount);
        imageViews = view.findViewById(R.id.image_view_dialog);

        btn_cancel = view.findViewById(R.id.dialog_btn_cancel);
        btn_save = view.findViewById(R.id.dialog_btn_save);
        btn_add_image = view.findViewById(R.id.ffb);

        /// set value to dialog layout
        title_dialog.setText(bookDao.getTitle());
        price_dialog.setText(String.valueOf(bookDao.getPrice()));
        discount_dialog.setText(String.valueOf(bookDao.getDecPrice()));
//        Glide.with(getContext()).load(bookDao.getThumbnail()).into(imageViews);

        setValueSpinner(spinner_dialog);
        header_dialog.setText("Update Book");


        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getValueDialog(Id);
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_add_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentImage = new Intent(Intent.ACTION_PICK);
                intentImage.setType("image/*");
                startActivityForResult(intentImage,CHOOSE_REQUEST_CODE);
            }
        });

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CHOOSE_REQUEST_CODE && requestCode != RESULT_OK && data != null){
            Toast.makeText(getContext(),"Run",Toast.LENGTH_LONG).show();
            uri = data.getData();
            imageViews.setImageURI(uri);
        }else{
            Toast.makeText(getContext(),"requestCode"+requestCode+"CHOOSE :"+CHOOSE_REQUEST_CODE+"RESULT_OK : "+RESULT_OK,Toast.LENGTH_LONG).show();
        }
    }

    public void  getValueDialog(int Id){
        String Title = title_dialog.getText().toString();
        String category = spinner_dialog.getSelectedItem().toString();
        int price =  Integer.parseInt(price_dialog.getText().toString());
        int total =  Integer.parseInt(discount_dialog.getText().toString());
        String imageUrl = uri.toString();

        if(Title.isEmpty() || category.isEmpty() || "".equals(price) || "".equals(total) || imageUrl.isEmpty()){
            Toast.makeText(getContext(),"Error",Toast.LENGTH_LONG).show();
        }else{
            BookDao bookDao =  new BookDao(Title,category,imageUrl,price,total);
            bookDao.setId(Id);
            RoomDataBaseConfig.getRoomDb(getContext()).bookInterface().updateBook(bookDao);
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
            Toast.makeText(getContext(),"Successfully",Toast.LENGTH_LONG).show();
        }

    }

    public void setValueSpinner(Spinner spinner){
        String[] BookDescription = new String[]{
                "JAVA Programming",
                "C# Programming",
                "Python Programming"
        };
        List<String> bookList = RoomDataBaseConfig.getRoomDb(getContext()).categoryInterface().findAllCategory();
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(),R.layout.support_simple_spinner_dropdown_item,bookList);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
    }

    public void getBookData(){
        bookDao = RoomDataBaseConfig.getRoomDb(getContext()).bookInterface().findAll();
        Log.d("TAG", "getBookData: "+bookDao.toString());
    }

}