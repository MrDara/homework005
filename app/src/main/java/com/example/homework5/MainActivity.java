package com.example.homework5;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.homework5.entity.BookDao;
import com.example.homework5.entity.CategoryDao;
import com.example.homework5.room.RoomDataBaseConfig;
import com.example.homework5.ui.DaskBoradFragment;
import com.example.homework5.ui.FavoriteFragment;
import com.example.homework5.ui.HomeFragment;
import com.example.homework5.ui.NotificationFragment;
import com.example.homework5.ui.ShareFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MainActivity extends AppCompatActivity {

     Toolbar toolbar;
     BottomNavigationView bottomNavigationView;

     TextView header_dialog;
     EditText title_dialog,price_dialog,discount_dialog;
     Spinner spinner_dialog;

     ImageView imageView;

     Button btn_save,btn_cancel;
     FloatingActionButton btn_add_image;

     AlertDialog dialog;

     String titleBook,categoryBook;
     int Book_price,Book_des_price;
     String Image_Book;

    Uri uri;

    List<CategoryDao> categoryDaoArrayList = new ArrayList<>();

    public final static int CHOOSE_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        setOnClickNavigation(this);
        openFragment(HomeFragment.newInstance("",""));

//        InitCategory();
//        DeleteCategory();
    }

    public void openFragment(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.Container,fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    public void setOnClickNavigation(final Context context){

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.navigation_home:
                        openFragment(HomeFragment.newInstance("",""));
                        Toast.makeText(context,"Home fragment",Toast.LENGTH_LONG).show();
                        return true;
                    case R.id.navigation_assessment:
                        openFragment(DaskBoradFragment.newInstance("",""));
                        Toast.makeText(context,"Assessment fragment",Toast.LENGTH_LONG).show();
                        return true;
                    case R.id.navigation_notifications:
                        openFragment(NotificationFragment.newInstance("",""));
                        Toast.makeText(MainActivity.this,"Notifications fragment",Toast.LENGTH_LONG).show();
                        return true;
                    case R.id.navigation_favorite:
                        openFragment(FavoriteFragment.newInstance("",""));
                        Toast.makeText(context,"Favorite fragment",Toast.LENGTH_LONG).show();
                        return true;
                    case R.id.navigation_share:
                        openFragment(ShareFragment.newInstance("",""));
                        Toast.makeText(context,"Share fragment",Toast.LENGTH_LONG).show();
                        return true;
                }
                return false;
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);

        MenuItem.OnActionExpandListener onActionExpandListener = new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                return true;
            }
        };

        menu.findItem(R.id.search).setOnActionExpandListener(onActionExpandListener);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setQueryHint("Search Data here .... ");
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                Toast.makeText(MainActivity.this,"Search : "+newText,Toast.LENGTH_LONG).show();
//                return false;
//            }
//        });

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.add:
                callDialogBox("Add New Book");
                break;
            case R.id.search:
                Toast.makeText(this,"Search",Toast.LENGTH_LONG).show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    public void callDialogBox(final String title){
        AlertDialog.Builder builderDialog;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            builderDialog = new AlertDialog.Builder(this,android.R.style.Theme_Material_Dialog_Alert);
        }else{
            builderDialog = new AlertDialog.Builder(this);
        }

        LayoutInflater inflater = getLayoutInflater();

        View view = inflater.inflate(R.layout.dailog_custom,null);

        header_dialog = view.findViewById(R.id.header_dialog);
        title_dialog = view.findViewById(R.id.edit_titlebook_dialog);
        spinner_dialog = view.findViewById(R.id.spinner_dialog_view);
        price_dialog = view.findViewById(R.id.edit_text_price);
        discount_dialog = view.findViewById(R.id.edit_text_discount);
        imageView = view.findViewById(R.id.image_view_dialog);

        btn_cancel = view.findViewById(R.id.dialog_btn_cancel);
        btn_save = view.findViewById(R.id.dialog_btn_save);
        btn_add_image = view.findViewById(R.id.ffb);

        setValueSpinner(spinner_dialog);
        header_dialog.setText(title);

        builderDialog.setView(view);
        builderDialog.setCancelable(true);
        dialog = builderDialog.create();


        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataFromDialog();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_add_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentImage = new Intent(Intent.ACTION_PICK);
                intentImage.setType("image/*");
                startActivityForResult(intentImage,CHOOSE_REQUEST_CODE);
            }
        });
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CHOOSE_REQUEST_CODE && requestCode != RESULT_OK && data != null){
            Toast.makeText(getApplicationContext(),"Run",Toast.LENGTH_LONG).show();
            uri = data.getData();
            imageView.setImageURI(uri);
        }else{
            Toast.makeText(getApplicationContext(),"requestCode"+requestCode+"CHOOSE :"+CHOOSE_REQUEST_CODE+"RESULT_OK : "+RESULT_OK,Toast.LENGTH_LONG).show();
        }
    }

    public void setValueSpinner(Spinner spinner){
//        String[] BookDescription = new String[]{
//                "JAVA Programming",
//                "C# Programming",
//                "Python Programming"
//        };
//        List<String> bookList = new ArrayList<>(Arrays.asList(BookDescription));

        List<String> CategoryList = RoomDataBaseConfig.getRoomDb(this).categoryInterface().findAllCategory();
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this,R.layout.support_simple_spinner_dropdown_item,CategoryList);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
    }


    public void getDataFromDialog(){

        try {
            titleBook = title_dialog.getText().toString();
            Book_price = Integer.parseInt(price_dialog.getText().toString());
            Book_des_price = Integer.parseInt(discount_dialog.getText().toString());
            Image_Book = uri.toString();
            categoryBook = spinner_dialog.getSelectedItem().toString();

            if(titleBook == null){
                Toast.makeText(getApplication(),"Invalid Title",Toast.LENGTH_LONG).show();
            }else if("".equals(Book_price)){
                Toast.makeText(getApplication(),"Invalid Price",Toast.LENGTH_LONG).show();
            }else if(categoryBook == null){
                Toast.makeText(getApplication(),"Invalid Category",Toast.LENGTH_LONG).show();
            }else if (Image_Book == null){
                Toast.makeText(getApplication(),"Invalid Image",Toast.LENGTH_LONG).show();
            }else if("".equals(Book_des_price)){
                Toast.makeText(getApplication(),"Invalid Total",Toast.LENGTH_LONG).show();
            }else{
                BookDao bookDao = new BookDao(titleBook,categoryBook,Image_Book,Book_price,Book_des_price);
                RoomDataBaseConfig.getRoomDb(getApplication()).bookInterface().insertBook(bookDao);
                dialog.dismiss();
                openFragment(HomeFragment.newInstance("",""));
            }
        }catch (Exception e){
            Toast.makeText(getApplication(),"Has Error",Toast.LENGTH_LONG).show();
        }

    }

    public void InitCategory(){
        RoomDataBaseConfig.getRoomDb(this).categoryInterface().insertCategory(
                    new CategoryDao("Java Programming"),
                    new CategoryDao("C# Programming"),
                    new CategoryDao("Python Programming")
                );
    }
    public void DeleteCategory(){
        List<CategoryDao> categoryDaos = RoomDataBaseConfig.getRoomDb(this).categoryInterface().findAll();
        for (int i=0;i<categoryDaos.size();i++){
            RoomDataBaseConfig.getRoomDb(this).categoryInterface().deleteCategory(categoryDaos.get(i));
        }
    }


}